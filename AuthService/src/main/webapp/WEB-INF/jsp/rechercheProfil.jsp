<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="header.jsp"></jsp:include>




<br />
<br />
<h2>Rechercher un profil client</h2>
<form action='/DevoCarWebPortail/list' method='post'>


    <div class="panel-body">

        <div class="form-group">
            <h5>
                <b>Adresse e-mail</b>
            </h5>
            <input type='email' name='email' class='form-control'
                   title=""/>

        </div>
        <div class="form-group">
            <h5>
                <b>Nom d'utilisateur </b>
            </h5>
            <input type='text' name='username' class='form-control'
                   title=""/>

        </div>
        <div class="form-group" id="lname" style="display: none">
            <h5>
                <b>Nom</b>
            </h5>
            <input type='text' name='lname' class='form-control'
                   title=""/>

        </div>
        <div class="form-group" id="fname" style="display: none">
            <h5>
                <b>Prenom</b>
            </h5>
            <input type='text' name='fname' class='form-control'
                   title=""/>

        </div>
        <div class="form-group" id="cp" style="display: none">
            <h5>
                <b>Code Postal</b>
            </h5>
            <input type='text' name='cp' class='form-control'
                   title=""/>

        </div>



        <div class="panel-footer" id="button">
            <button type="submit" class="btn btn-warning">Rechercher</button>
            <button type="button"  class=pull-right class="btn btn-warning pull-right" onclick="rechercheAv()">Recherche avancee</button>
        </div>
    </div>
</form>

<br />

<h3>Profil</h3>

<table class='table table-hover table-responsive table-bordered'>

    <tr>
<%--
        <th data-field="username" data-filter-control="input" data-sortable="true">Nom d'utilisateur</th>
--%>
        <th>Nom d'utilisateur</th>
        <th>Email</th>
        <th>Genre</th>
        <th>Nom</th>
        <th>Prenom</th>
        <th>Code Postal</th>
        <th>Profil</th>
        <th>Type d'abonnement</th>
        <th>Action</th>
    </tr>

    <c:forEach items="${profiles}" var="profiles">
        <tr>
            <td>${profiles.username}</td>
            <td>${profiles.mail}</td>
            <td>${profiles.sex}</td>
            <td>${profiles.fname}</td>
            <td>${profiles.lname}</td>
            <td>${profiles.cp}</td>
            <td>${profiles.profile}</td>
            <td>${profiles.abo}</td>
            <td>

    <button class="btn btn-info" onclick="document.location.href='profilesShow?username=${profiles.username}'">Show</button>

            </td>
        </tr>

    </c:forEach>

</>

<script>

    function rechercheAv() {
        if (document.getElementById("lname").style.display === "none") {
            document.getElementById("lname").style.display = "";
            document.getElementById("fname").style.display = "";
            document.getElementById("cp").style.display = "";

        } else {
            document.getElementById("lname").style.display = "none";
            document.getElementById("fname").style.display = "none";
            document.getElementById("cp").style.display = "none";

        }
    }

</script>






<jsp:include page="footer.jsp"></jsp:include>