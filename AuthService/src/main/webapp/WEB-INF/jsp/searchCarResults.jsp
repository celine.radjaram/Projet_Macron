<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.webPortail.springboot.model.Car" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: louisendelicher
  Date: 09/01/2018
  Time: 23:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"></jsp:include>
</br></br></br>

<ul class="nav nav-tabs">
    <li role="presentation"><a href="/DevoCarWebPortail/addcar"><i class="glyphicon glyphicon-plus" aria-hidden="true"></i>&nbsp;Créer</a></li>
    <li role="presentation" class="active"><a href="/DevoCarWebPortail/searchcar"><i class="glyphicon glyphicon-search" aria-hidden="true"></i>&nbsp;Rechercher</a></li>
</ul>
<div style="padding: 0 40px 20px;">
    <h1>
        <a href="/DevoCarWebPortail/searchcar" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span>&nbsp;Retour</a>&nbsp;Véhicules trouvés&nbsp;<span class="badge">${carList.size()}</span>
    </h1><br/>

    <c:choose>
        <c:when test="${carList == null}">
            <div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>&nbsp;Aucun véhicule trouvé avec ce(s) critère(s)
            </div>
        </c:when>
        <c:otherwise>
            <div class="alert alert-info alert" role="alert">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                Il est possible de cliquer sur certains éléments pour obtenir les véhicules avec cette caractéristique (MARQUE, MODÈLE...)
            </div>
        </c:otherwise>
    </c:choose>

    <c:forEach items="${carList}" var="car">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="pull-left"><button class="btn btn-default disabled"><span class="glyphicon glyphicon-barcode"></span>&nbsp;${car.id}</button>&nbsp;<button class="btn btn-warning disabled">${car.plateNumber}</button></div>
                <div class="pull-right">
                    <a href="/DevoCarWebPortail/car/brand/${car.marque}" class="btn btn-primary">${car.marque}</a>
                    <a href="/DevoCarWebPortail/car/model/${car.modele}" class="btn btn-info">${car.modele}</a>
                    <a href="/DevoCarWebPortail/car/category/${car.categorie}" class="btn btn-success">${car.categorie}</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <h4><b>Nombre de places :</b> <a href="/DevoCarWebPortail/car/places/${car.nombrePlaces}" title="Nombre de places" class="btn btn-default">${car.nombrePlaces}</a></h4>
                <h4><b>Dimensions :</b> <a href="/DevoCarWebPortail/car/length/${car.longueur}" title="Longueur" class="btn btn-default"><span class="glyphicon glyphicon-resize-vertical"></span>&nbsp;${car.longueur}</a> x <a href="/DevoCarWebPortail/car/width/${car.largeur}" title="Largeur" class="btn btn-default"><span class="glyphicon glyphicon-resize-horizontal"></span>&nbsp;${car.largeur}</a></h4>
            </div>
        </div>
    </c:forEach>
</div>