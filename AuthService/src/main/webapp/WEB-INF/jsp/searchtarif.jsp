<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<jsp:include page="header.jsp"></jsp:include>
</br>
</br>
</br>

<div class="container">
	
<ul class="nav nav-pills">
	<li role="presentation" class="active"><a
		href="/DevoCarWebPortail/tarif"><i
			class="glyphicon glyphicon-plus" aria-hidden="true"></i>&nbsp;Créer</a></li>

	<li role="presentation" class="active"><a
		href="/DevoCarWebPortail/searchtarif"><i
			class="glyphicon glyphicon-search" aria-hidden="true"></i>&nbsp;Rechercher</a></li>
</ul>
<div>
	<h3><strong>Rechercher un Tarif</strong></h3>
	<form action='/DevoCarWebPortail/tarifsearchresult' method='get'>
	
		<div class="alert alert-info">
			La recherche est multi critère, vous pouvez rechercher un tarif par : Libellé, type de véhicule et type de location
		</div>
		<tr>
			<td><strong>Type de Véhicule</strong></td>
			<!-- Fill with data from vehicules when READ OK-->
			<td><select name="SearchtypeVehicule">
					<option value="Voiture">Voiture</option>
					<option value="vélo" selected>Vélo</option>
					<option value=""></option>
			</select></td>
		</tr>


		</br> </br>
		<tr>
			<td><b><strong>Type de location</strong></b></td>
			
			<td><select name="Searchtypelocation">
					<option value="Voiture">Voiture</option>
					<option value="Station" selected>Station</option>
					<option value="Groupe" selected>Groupe</option>
					<option value=""></option>
			</select></td>
		</tr>


		</br> <br>
		<tr>
			<td><b><strong>Libellé Tarif</strong></b></td>
			</br>
			<td><input type='text' name='search' class='form-control'  title='Veuillez entrer des lettres seulement'
				pattern="[a-zA-Z ]*"
			/></td>
		</tr>
		<td>
			<button type="submit">Valider</button>
		</td>
	</form>
	<br> 
	<h3><strong>Rechercher un Abonnement</strong></h3>
	<form action='/DevoCarWebPortail/abonnementsearchresult' method='get'>

		<tr>
			<td><b><strong>Libellé abonement</strong></b></td>
			</br>
			<td><input type='text' name='search' class='form-control'
				required  title='Veuillez entrer des lettres seulement'
				pattern="[a-zA-Z ]*" /></td>
		</tr>
		<td>
			<button type="submit">Valider</button>
		</td>
	</form>
</div>
</div>

<jsp:include page="footer.jsp"></jsp:include>

