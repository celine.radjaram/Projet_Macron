

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.webPortail.springboot.model.Tarif"%>
<%@ page import="java.util.List"%><%--

--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<jsp:include page="header.jsp"></jsp:include>

<ul class="nav nav-tabs">
	<li role="presentation"><a href="/DevoCarWebPortail/tarif"><i
			class="glyphicon glyphicon-plus" aria-hidden="true"></i>&nbsp;Créer</a></li>
	<li role="presentation" class="active"><a
		href="/DevoCarWebPortail/searchtarif"><i
			class="glyphicon glyphicon-search" aria-hidden="true"></i>&nbsp;Rechercher</a></li>
</ul>

<h1>
	<a href="/DevoCarWebPortail/searchtarif" class="btn btn-default"><span
		class="glyphicon glyphicon-chevron-left"></span>&nbsp;Rechercher un
		autre tarif</a>&nbsp;Résultat de la recherche&nbsp;
</h1>
<br />

<c:choose>
	<c:when test="${tarifs == null}">
		<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-option-horizontal"
				aria-hidden="true"></span>${erreur}
		</div>
	</c:when>
	<c:otherwise>
		<div class="alert alert-success" role="alert">
			<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
			Liste des tarifs trouvés
		</div>
	</c:otherwise>
</c:choose>


<div class="container">
<div class="row">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Libelle</th>
				<th>Prix horaire</th>
				<th>Type vehicule</th>
				<th>Type location</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${tarifs}" var="tarif">
				<tr>
					<td>${tarif.libelle}</td>
					<td>${tarif.prixHoraire}</td>
					<td>${tarif.typeVehicule}</td>
					<td>${tarif.typeLocation}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
</div>






