<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<jsp:include page="header.jsp"></jsp:include>

</br></br></br>
<ul class="nav nav-tabs">
    <li role="presentation"><a href="/DevoCarWebPortail/place/viewplaces"><i class="glyphicon glyphicon-plus" ></i>&nbsp;Consulter la liste des places de parking</a></li>
    <li role="presentation"><a href="/DevoCarWebPortail/place/findPlace"><i class="glyphicon glyphicon-search" ></i>&nbsp;Rechercher une place de parking</a></li>
    <li role="presentation" class="active"><a href="/DevoCarWebPortail/place"><i class="glyphicon glyphicon-search" ></i>&nbsp;Ajouter une place de parking</a></li>
</ul>
<div style="padding: 20px 40px;">
	<h1>Ajouter une place de parking</h1>
	<form action='/DevoCarWebPortail/place/add' method="post">
	<div class="panel-body">
			<div class="form-group">
				<label >Code Postal :</label>
				<input type="text" name="codePostal" class='form-control'
					required pattern="[0-9]{5}"
					title='Le code postal ne doit contenir que 5 chiffres.' />
			</div>
			<div class="form-group">
				<label>Commune/Ville:</label>
				<input type="text" name="commune" class='form-control'
					required pattern="[a-z-&#x2010-A-Z]*&#160[a-z-&#x2010-A-Z]"
					title='La commune/ville ne doit pas contenir des numeros et des caracteres speciaux.' />
			</div>
			<div class="form-group">
				<label>Numero:</label>
				<input type="text" name="numero" class='form-control'
					required pattern="[0-9]+"
					title='Le numero ne doit pas contenir des alphabets et des caracteres speciaux.' />
			</div>
			<div class="form-group">
				<label>Rue:</label>
				<input type="text" name="rue" class='form-control' required
					pattern="[a-z-&#x2010-A-Z]*&#160[a-z-&#x2010-A-Z]*"
					title='La rue ne doit pas contenir des numeros et des caracteres speciaux.' />
			</div>
			<div class="form-group">
				<label>Latitude:</label>
				<input type="text" name="latitude" class='form-control'
					required pattern = "[0-9]+.[0-9]*"
					title='La latitude doit etre ecrite au format degré décimaux.'/>
					</div>
			<div class="form-group">
				<label>Longitude:</label>
				<input type="text" name="longitude" class='form-control'
					required pattern = "[0-9]+.[0-9]*"
					title='La longitude doit etre ecrite au format degré décimaux.' />
			</div>
			<div class="form-group">
				<label>Longueur:</label>
				<input type="text" name="longueur" class='form-control'
					required pattern="[0-9]+"
					title='Veulliez entrer des chiffres seulement.' />
			</div>
			<div class="form-group">
				<label>Largeur:</label>
				<input type="text" name="largeur" class='form-control'
					required pattern="[0-9]+"

					title='Veulliez entrer des chiffres seulement.' />
			</div>
			<div class="panel-footer">
				<button type="submit" class="btn btn-primary">Valider</button>
			</div>
			</div>
		<b><c:out value="${message}"></c:out></b> <b><c:out
				value="${danger}"></c:out></b>

	</form>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
