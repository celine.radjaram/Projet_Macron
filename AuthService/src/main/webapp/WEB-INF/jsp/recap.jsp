<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="header.jsp"></jsp:include><html lang="en">
<br />
<br />
<br />
<body>

	<h3>Confirmez-vous ces informations ?</h3>
	Votre nom d'utilisateur est ${username}.
	<br /> Votre genre : ${sex}.
	<br /> Vous vous appelez ${fname} ${lname}.
	<br /> Nous pourrons vous contacter par e-mail au ${mail} ou par telephone au ${num}.
	<br /> Vous etes ne(e) le ${bday}.
	<br /> Vous habitez au ${numaddress} ${nameaddress}, ${cp}.
	<script>
if(${numaddressfav}!=null){
	document.write("<br /> Vous avez choisi une/des adresse(s) preferee(s) qui se situe(nt) au :");
	document.write("<br /> ${numaddressfav} ${nameaddressfav}, ${cpfav}");
			document.write("<br /> ${numaddressfav2} ${nameaddressfav2}, ${cpfav2}");
}</script>
<script>
			document.write("<br /> Votre profil est ");
if(${profile}==true){
	console.log(${profile});
	document.write("public (d'autres utilisateurs pourront, par exemple, vous ajouter dans un groupe de client).");
}else{
	document.write("prive.");
}
	</script>
	<br /> Vous etes un locataire de vehicules ${abo}.
	<br />
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<form:form action='/DevoCarWebPortail/profileCreated' method='get'>
				<button class="btn-lg btn-primary pull-right">Confirmer</button>
			</form:form>
			<a href="javascript:history.go(-1)"><button
					class="btn-lg btn-primary pull-right">Corriger</button></a>
		</div>
	</div>

	<jsp:include page="footer.jsp"></jsp:include>