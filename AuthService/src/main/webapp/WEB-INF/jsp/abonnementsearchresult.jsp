



<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.webPortail.springboot.model.Abonnement"%>
<%@ page import="java.util.List"%><%--

--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<jsp:include page="header.jsp"></jsp:include>

<ul class="nav nav-tabs">
	<li role="presentation"><a href="/DevoCarWebPortail/tarif"><i
			class="glyphicon glyphicon-plus" aria-hidden="true"></i>&nbsp;Créer</a></li>
	<li role="presentation" class="active"><a
		href="/DevoCarWebPortail/searchtarif"><i
			class="glyphicon glyphicon-search" aria-hidden="true"></i>&nbsp;Rechercher</a></li>
</ul>

<h1>
	<a href="/DevoCarWebPortail/searchtarif" class="btn btn-default"><span
		class="glyphicon glyphicon-chevron-left"></span>&nbsp;Rechercher un
		autre tarif</a>&nbsp;Résultat de la recherche&nbsp;
</h1>
<br />

<c:if test="${empty abonnements}">
	<div class="alert alert-danger" role="alert">
		<span class="glyphicon glyphicon-option-horizontal" aria-hidden="true"></span>Aucun entregistrement trouvé
	</div>
</c:if>
<c:if test="${!empty abonnements}">
	<div class="alert alert-info alert" role="alert">
		<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
		Liste des tarifs abonnement trouvés
	</div>
</c:if>




<div class="container">
	<div class="row">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Libellé</th>
					<th>Prix horaire</th>

				</tr>
			</thead>
			<tbody>
				<c:forEach items="${abonnements}" var="abonnement">
					<tr>
						<td>${abonnement.libelle}</td>
						<td>${abonnement.prix}</td>

					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
