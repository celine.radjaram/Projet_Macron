<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="header.jsp"></jsp:include>
</br></br></br>

<ul class="nav nav-tabs">
    <li role="presentation" class="active"><a href="/DevoCarWebPortail/addcar"><i class="glyphicon glyphicon-plus"
                                                                                  aria-hidden="true"></i>&nbsp;Créer</a>
    </li>
    <li role="presentation"><a href="/DevoCarWebPortail/searchcar"><i class="glyphicon glyphicon-search"
                                                                      aria-hidden="true"></i>&nbsp;Rechercher</a></li>
</ul>
<div style="padding: 20px 40px;">
    <c:choose>
        <c:when test="${!empty info}">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
                <c:out value='${info}' />
            </div>
        </c:when>
        <c:when test="${!empty error}">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
                <c:out value='${error}' />
            </div>
        </c:when>
    </c:choose>
    <div class="alert alert-info alert" role="alert">
        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
        Tous les champs sont obligatoires
    </div>
    <form action='/DevoCarWebPortail/car/add' autocomplete="off" method='post'>
        <div>
            <div class="form-group">
                <label>Marque :</label>
                <input type="text" name='marque' class="form-control" placeholder="Entrez la marque du véhicule">
            </div>

            <div class="form-group">
                <label>Modèle :</label>
                <input type="text" name='modele' class="form-control" placeholder="Entrez le modèle du véhicule">
            </div>

            <div class="form-group">
                <label>Catégorie :</label>
                <select name='categorie' class="form-control">
                    <option disabled selected>Sélectionnez la catégorie</option>
                    <option>Berline</option>
                    <option>Break</option>
                    <option>Citadine</option>
                    <option>Coupé</option>
                    <option>Monospace</option>
                </select>
            </div>

            <div class="form-group">
                <label>Nombre de places :</label>
                <select name='nombrePlaces' class="form-control">
                    <option disabled selected>Sélectionnez le nombre de places</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                </select>
            </div>

            <div class="form-group">
                <label>Longeur :</label>
                <input type="text" pattern="\d{1}|\d{1}[\.]\d{1}|\d{1}[\.]\d{2}" name='longueur' placeholder="Entrez la longueur du véhicule (x ou x.x ou x.xx)" step="0.01"
                       class="form-control">
            </div>

            <div class="form-group">
                <label>Largeur :</label>
                <input type="text" pattern="\d{1}|\d{1}[\.]\d{1}|\d{1}[\.]\d{2}" name='largeur' placeholder="Entrez la largeur du véhicule (x ou x.x ou x.xx)" step="0.01"
                       class="form-control">
            </div>

            <div class="form-group">
                <label>Numéro de plaque :</label>
                <input type="text" pattern="[A-NP-Z]{2}-[0-9]{3}-[A-NP-Z]{2}" name='plateNumber'
                       placeholder="Entrez la plaque d'immatriculation (AA-000-AA) " title="(AA-000-AA)"
                       class="form-control">
            </div>
        </div>

        <button type="submit" class="btn btn-warning">Créer</button>
    </form>


</div>
<jsp:include page="footer.jsp"></jsp:include>
