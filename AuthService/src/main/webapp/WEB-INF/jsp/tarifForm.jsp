<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="header.jsp"></jsp:include>
</br></br></br>

<ul class="nav nav-pills">
	<li role="presentation" class="active"><a href="/DevoCarWebPortail/tarif"><i class="glyphicon glyphicon-plus" aria-hidden="true"></i>&nbsp;Créer</a></li>

	<li role="presentation" class="active"><a href="/DevoCarWebPortail/searchtarif"><i class="glyphicon glyphicon-search" aria-hidden="true"></i>&nbsp;Rechercher</a></li>
</ul>
<div>

<body>

<h3>Créer un Tarif</h3>
<h2>Tarif Véhicules</h2>

<br>
<form action='/DevoCarWebPortail/tarif/add' method='post'>
    <table class='table table-hover table-responsive table-bordered'>
        <tr>
            <td><b>Libelle</b></td> 
            <td><input type='text' name='libelle' class='form-control' required 
            title="Veuillez entrer des lettres seulement"
            pattern="[a-zA-Z ]*"/></td>
        </tr>
        <tr>
            <td><b>Prix Horaire</b></td>
            <td><input type='number' name='prixHoraire' class='form-control' min="1" required title="Veuillez remplir ce champ"/></td>
        </tr>
        <tr>
            <td><b>Type Véhicule</b></td>
            <!-- Fill with data from vehicules when READ OK-->
            <td><select name="typeVehicule">
  					<option value="Voiture">Voiture</option> 
  					<option value="Vélo" selected>Vélo</option>
  					
				</select>
			</td>
        </tr>
 		<tr>
            <td><b>Type Location</b></td>
            <!-- Fill with data from tarifs when READ OK -->
            <td><select name="typeLocation">
  					<option value="Voiture">Voiture</option> 
  					<option value="Station">Station</option> 
  					<option value="Groupe" selected>Groupe</option>
				</select>
			</td>
        </tr>
        <tr>
            <td></td>
            <td>
                <button type="submit" class="btn btn-primary">Valider</button>
            </td>
        </tr>
    </table>
    <b><c:out value="${message}"></c:out></b>
    <b><c:out value="${danger}"></c:out></b>
</form>
</b>
<h2>Tarif Abonnement</h2>
<form action='/DevoCarWebPortail/abonnement/add' method='post'>
    <table class='table table-hover table-responsive table-bordered'>
        <tr>
            <td><b>Libelle</b></td> 
            <td><input type='text' name='libelle' class='form-control' 
            required title='Veulliez entrer des lettres seulement'
            pattern="[a-zA-Z ]*"/></td>
        </tr>
        <tr>
            <td><b>Prix</b></td>
            <td><input type='number' name='prix' class='form-control' min="1" title='Veuillez remplir ce champ'/></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <button type="submit" class="btn btn-primary">Valider</button>
            </td>
        </tr>
    </table>
    <b><c:out value="${message}"></c:out></b>
    <b><c:out value="${danger}"></c:out></b>
</form>

<jsp:include page="footer.jsp"></jsp:include>