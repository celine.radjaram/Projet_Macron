package com.webPortail.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages={"com.webPortail.springboot"})
public class AuthService {

	public static void main(String[] args) {
		
		SpringApplication.run(AuthService.class, args);
	}
}
