package com.webPortail.springboot.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="authentification")
public class Authentification implements Serializable{
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="username")
	private String username;
	
	@Column(name="password")
	private String password;
	
	public Authentification() {}
	
	public Authentification(String username, String password) {
		this.username= username;
		this.password = password;
	}
	

	public String getUsername() {
		return this.username;
	}
	
	public void setUsername(String newUsername) {
		this.username = newUsername;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public void setPassword(String newPassword) {
		this.password = newPassword;
	}
	
	@Override
	public String toString() {
		return "authentification [username=" + username + ", password=" + password + "]";
	}

}
