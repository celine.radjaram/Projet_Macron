package fr.macron;

import model.Tweet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;
import repositories.*;

@SpringBootApplication
public class Scrapper {

    @Autowired
    private ITweetRepository tweetRepository;

    private static ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();


    //Paramètres d'authentification
    private String OAuthConsumerKey = "WHyr9UEreoGbrjY3IJRiBOFgF";
    private String OAuthConsumerSecret = "yj6xqVsBWRT3ObGuiTPOYGtR1Z4fhZaupanP9vdWCNkBrXvRH5";
    private String OAuthAccessToken = "881973172995846146-sc2S8GGwgL9y8cO09832QfQGMcbFx69";
    private String OAuthAccessTokenSecret = "uVvb6SI8FEZ1v6BDQ1FG8so5IxrruuBH2tb03AMjk6ttr";

    public static void main(String[] args) {
        Scrapper scrap = new Scrapper();
        scrap.twitterListener();
    }

    //Utilisation des paramètres d'authentification pour se connecter à l'API Twitter et utiliser le stream
    private TwitterStream twitterConnection(boolean debug) {
        Scrapper.configurationBuilder
                .setDebugEnabled(debug)
                .setOAuthConsumerKey(OAuthConsumerKey)
                .setOAuthConsumerSecret(OAuthConsumerSecret)
                .setOAuthAccessToken(OAuthAccessToken)
                .setOAuthAccessTokenSecret(OAuthAccessTokenSecret);

        TwitterStream twitterStream = new TwitterStreamFactory(Scrapper.configurationBuilder.build()).getInstance();
        return twitterStream;
    }


    //Recherche des tweets selon un filtre
    public void twitterListener() {
        TwitterStream twitterStream = twitterConnection(true);

        StatusListener listener = new StatusListener() {


            @Override
            public void onStatus(Status status) {
                //Affichage du message du tweet
                System.out.println("MSG : " + status.getText());
                //Affichage du pseudo de l'utilisateur du tweet
                System.out.println("PSEUDO : " + status.getUser().getScreenName());
                //Affichage de la date du tweet
                System.out.println("DATE : " + status.getCreatedAt());

                Tweet scrapperTweet = new Tweet();
                scrapperTweet.setTweet(status.getText());
                scrapperTweet.setDate(status.getCreatedAt());
                scrapperTweet.setAuteur(status.getUser().getScreenName());

                tweetRepository.save(scrapperTweet);

            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice arg) { }

            @Override
            public void onScrubGeo(long l, long l1) { }

            @Override
            public void onStallWarning(StallWarning warning) { }

            @Override
            public void onTrackLimitationNotice(int i) { }


        };

        //Filtre sur "Macron"
        twitterStream.addListener(listener);
        twitterStream.filter("Macron");
    }


}
