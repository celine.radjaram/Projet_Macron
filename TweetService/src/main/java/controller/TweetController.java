package controller;

import model.Tweet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import service.ITweetService;

import java.util.List;

@CrossOrigin
@Controller
@RestController
public class TweetController {

    private static final Logger logger = LoggerFactory.getLogger(TweetController.class);

    @Autowired
    ITweetService tweetService;

    //Affiche les tweets en format Json
    @RequestMapping(value = "/accueil", method = RequestMethod.GET)
    public List<Tweet> showHome() {
        logger.info("TweetController#showHome");
        return tweetService.findAllTweets();
    }

    //Affiche une jsp, ne marche pas
    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public ModelAndView showForm() {
        ModelAndView mav = new ModelAndView("accueil");
        return mav;
    }

}
