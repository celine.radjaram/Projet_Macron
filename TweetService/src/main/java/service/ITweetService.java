package service;

import model.Tweet;

import java.util.List;

public interface ITweetService {

    void saveTweet(Tweet tweet);

    Tweet searchTweetById(String id);

    List<Tweet> findAllTweets();

}
