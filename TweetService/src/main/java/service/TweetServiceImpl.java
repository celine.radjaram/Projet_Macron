package service;

import model.Tweet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import repositories.ITweetRepository;

import java.util.List;

@Service
@Transactional
public class TweetServiceImpl implements ITweetService {

    @Autowired
    private ITweetRepository tweetRepository;

    //Enregistre un tweet dans la BD
    @Override
    public void saveTweet(Tweet tweet) {
        tweetRepository.save(tweet);
    }

    //Retourne un tweet recherché par son id
    @Override
    public Tweet searchTweetById(String id) {
        return this.tweetRepository.findById(Integer.parseInt(id));
    }

    //Retourne la liste des tweets contenue dans la BD
    @Override
    public List<Tweet> findAllTweets() {
        return tweetRepository.findAll();
    }
}