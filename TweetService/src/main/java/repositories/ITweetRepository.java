package repositories;

import model.Tweet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ITweetRepository extends JpaRepository<Tweet, Long> {
    Tweet findById(int id);

}
