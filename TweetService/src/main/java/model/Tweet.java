package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "Tweet")
public class Tweet implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "date")
    private Date date;

    @Column(name = "auteur")
    private String auteur;

    @Column(name = "tweet")
    private String tweet;


    public void setId(int newId) {
        this.id = newId;
    }

    public void setDate(Date newDate) {
        this.date = newDate;
    }

    public void setAuteur(String newAuteur) {
        this.auteur = newAuteur;
    }

    public void setTweet(String newTweet) {
        this.tweet = newTweet;
    }


}
